# Build modified Debian images

This simple bash script extracts the contents of a Debian iso file, integrates a preseed.cfg file and packs everything up again into an iso file. Simple as that.

## How to

Put a Debian iso file into the `iso/` folder, create a preseed configuration in `pressed/` (or use or modify one of the existing files) and launch `build_iso.sh`.
The script copies the contents of the iso file to `iso/cd`. Feel free to modify those files (e.g. make changes to the bootloader), they will be incorporated into the to-be-created new iso file.

The newly generated iso file will be placed under `iso/`.

**BE AWARE**: 

* The script does not use ANY safety checks or error handling! Use at your own risk!
* If you use the provied preseed configurations: They will produce a fully automatic installation, no questions asked, harddisks will be wiped! You have been **warned**!
