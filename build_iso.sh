#!/bin/bash

iso_files=()
for file in iso/*.iso
do
    if [ -f "${file}" ]; then
        iso_files+=("${file}")
    fi
done

preseed_files=()
for file in preseed/*.cfg
do
    if [ -f "${file}" ]; then
        preseed_files+=("${file}")
    fi
done


echo -e "==== Debian ISO Builder ====\n"
echo -e "Which ISO file do you want to use?\n"

for ((i = 0; i < ${#iso_files[@]}; i++)); do
    echo "(${i}) ${iso_files[$i]}"
    
done
echo
read -p "Please select a number and press [Enter]: " user_iso

echo -e "Which preseed file do you want to use?\n"

for ((i = 0; i < ${#preseed_files[@]}; i++)); do
    echo "(${i}) ${preseed_files[$i]}"
    
done
echo
read -p "Please select a number and press [Enter]: " user_preseed

# Mount ISO and copy content
mkdir iso/mount
mkdir preseed/tmp

cp "${preseed_files[${user_preseed}]}" preseed/tmp/preseed.cfg

sudo mount -o loop "${iso_files[${user_iso}]}" iso/mount
mkdir iso/cd
rsync -a -H --exclude=TRANS.TBL iso/mount/ iso/cd

sudo gunzip iso/cd/install.amd/initrd.gz
echo preseed/tmp/preseed.cfg | sudo cpio -o -H newc -A -F iso/cd/install.amd/initrd

echo -e "\n###################\n##### ATTENTION #####\n###################\n"
read -p "In case you want to modify other files included in the ISO, please do your changes in 'iso/cd'. Press [Enter] to continue!"

sudo gzip iso/cd/install.amd/initrd
cd iso/cd
sudo md5sum $(find -follow -type f) | sudo tee md5sum.txt
cd ..
sudo genisoimage -o $(basename "${iso_files[${user_iso}]}"-$(basename -s .cfg "${preseed_files[${user_preseed}]}")-$(date +%Y%m%d%H%M)) -r -J -no-emul-boot -boot-load-size 4 -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat ./cd
#cp debian9-auto-minimal-custom.iso /var/www/docroot_apache/

cd ..
sudo umount iso/mount
sudo rm -r iso/mount iso/cd preseed/tmp
